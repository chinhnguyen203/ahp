drop database if exists ahp;
create database ahp;
use ahp;

-- USER --
drop table if exists user;
create table user(
	id int primary key auto_increment,
    full_name nvarchar(30) not null,
    user_name nvarchar(20) not null,
    password nvarchar(20) not null,
    user_role bit COMMENT '1 - admin, 0 - user'
);

-- PROBLEM --
drop table if exists problem;
create table problem(
	id int primary key auto_increment,
    problem_name nvarchar(255) not null COMMENT 'ten bai toan',
    user_id int not null COMMENT 'ma user'
) COMMENT 'Bang bai toan';

-- PLAN --
drop table if exists plan;
create table plan(
	id int primary key auto_increment,
    plan_name nvarchar(255) not null COMMENT 'ten phuong an',
    plan_score int not null COMMENT 'diem phuong an',
    plan_quantity int not null COMMENT 'so phuong an',
    criteria_id int not null COMMENT 'ma tieu chi'
) COMMENT 'Bang phuong an';

-- CRITERIA --
drop table if exists criteria;
create table criteria(
	id int primary key auto_increment,
    criteria_name nvarchar(255) not null COMMENT 'ten tieu chi',
    weight int not null COMMENT 'trong so',
    critical_level int not null COMMENT 'muc do quan trong',
    criteria_quantity int not null COMMENT 'so tieu chi',
    problem_id int not null COMMENT 'ma bai toan',
    consistency_ratio_id int not null COMMENT 'ty so nhat quan',
    priority_id int not null COMMENT 'muc do uu tien'
) COMMENT 'Bang tieu chi';

-- PRIORITY --
drop table if exists priority;
create table priority(
	id int primary key auto_increment,
    priority_level int not null COMMENT 'do uu tien',
    value int not null COMMENT 'gia tri so'
) COMMENT 'Bang do uu tien';

-- CONSISTENCY RATIO --
drop table if exists consistency_ratio;
create table consistency_ratio(
	id int primary key auto_increment,
    cr int not null,
    ci int not null,
    ri int not null,
    lamda_max int not null
) COMMENT 'Bang ty so nhat quan';