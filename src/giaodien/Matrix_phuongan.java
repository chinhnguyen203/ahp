package giaodien;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import model.Criteria;
import model.Plan;
import run.RunAHP;

public class Matrix_phuongan extends javax.swing.JFrame {

    int numberPlan = 1;
    int numberCriteria = 5;
    List<Plan> listPlan = new ArrayList<>();
    List<Criteria> listCriterias = new ArrayList<>();

    List<Double> listFirst = new ArrayList<>();
    List<Double> listSecond = new ArrayList<>();
    List<Double> listThird = new ArrayList<>();
    List<Double> listFour = new ArrayList<>();
    List<Double> listFive = new ArrayList<>();

    public Matrix_phuongan() {
        initComponents();

    }

    public void setNumberCriteria(int numberCriteria) {
        this.numberCriteria = numberCriteria;
    }

    public void setNumberPlan(int numberPlan) {
        this.numberPlan = numberPlan;
    }

    public void setListPlan(List<Plan> listPlan) {
        this.listPlan = listPlan;
        initMatrix();
    }

    public void setListCriterias(List<Criteria> listCriterias) {
        this.listCriterias = listCriterias;
    }

    private void initMatrix() {
        Object[] colNames = new Object[listCriterias.size() > 0 ? listCriterias.size() + 1 : 5];
        colNames[0] = " ";
        for (int i = 1; i < colNames.length; i++) {
            if (listCriterias.size() > 0) {
                colNames[i] = listCriterias.get(i - 1).getNameCriteria();
            } else {
                colNames[i] = "Cột " + i;
            }
        }
        Object[][] data = new Object[listPlan.size()][colNames.length];
        int i = 0;
        for (Plan s : listPlan) {
            data[i][0] = s.getNamePlan();
            i++;
        }
        TableModel tbModel = new DefaultTableModel(data, colNames);
        JTableHeader tableHeader = tableForm.getTableHeader();
        Font headerFont = new Font("Verdana", Font.BOLD, 14);
        tableHeader.setFont(headerFont);
        tableForm.setModel(tbModel);
        tableForm.setShowGrid(true);
        tableForm.setRowHeight(24);

        tableForm.setDefaultRenderer(Double.class, new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                c.setForeground(((Double) value) > 10 ? Color.BLUE : Color.RED);
                return c;
            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableForm = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ma trận phương án ");

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Next");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        tableForm.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tableForm.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tableForm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableForm);
        if (tableForm.getColumnModel().getColumnCount() > 0) {
            tableForm.getColumnModel().getColumn(0).setResizable(false);
        }

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Nhập số liệu các phương án");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.getAccessibleContext().setAccessibleName("jLable1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 555, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(34, 34, 34)
                        .addComponent(jButton2)))
                .addGap(29, 29, 29))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(240, 240, 240))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(21, 21, 21))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private boolean importDataFromTable() {
        if (listFirst.size() > 0) {
            listFirst.clear();
        }
        if (listSecond.size() > 0) {
            listSecond.clear();
        }
        if (listThird.size() > 0) {
            listThird.clear();
        }
        if (listFour.size() > 0) {
            listFour.clear();
        }
        if (listFive.size() > 0) {
            listFive.clear();
        }
        try {
            DefaultTableModel modelDefaultTableModel = (DefaultTableModel) tableForm.getModel();
            for (int i = 0; i < tableForm.getRowCount(); i++) {
                switch (listCriterias.size()) {
                    case 1:
                        listFirst.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 1).toString()));
                        break;
                    case 2:
                        listFirst.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 1).toString()));
                        listSecond.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 2).toString()));
                        break;
                    case 3:
                        listFirst.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 1).toString()));
                        listSecond.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 2).toString()));
                        listThird.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 3).toString()));
                        break;
                    case 4:
                        listFirst.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 1).toString()));
                        listSecond.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 2).toString()));
                        listThird.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 3).toString()));
                        listFour.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 4).toString()));
                        break;
                    case 5:
                        listFirst.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 1).toString()));
                        listSecond.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 2).toString()));
                        listThird.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 3).toString()));
                        listFour.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 4).toString()));
                        listFive.add(Double.parseDouble(modelDefaultTableModel.getValueAt(i, 5).toString()));
                        break;
                }

            }
            return true;
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(this, "Please enter values!");
            return false;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Please enter number!");
            return false;
        }

    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        setVisible(false);
        dispose();
        Nhap_so_phuongan phuongan = new Nhap_so_phuongan();
        phuongan.show();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (importDataFromTable()) {
            setVisible(false);
            dispose();
            ketqua ketqua = new ketqua();
            ketqua.setListPlan(listPlan);
            ketqua.setListCriterias(listCriterias);
            double weight = listCriterias.get(0).getWeight();
            double weight1 = listCriterias.get(1).getWeight();
            double weight2 = listCriterias.get(2).getWeight();
            double weight3, weight4;
            try {
                weight3 = listCriterias.get(3).getWeight();
            } catch (IndexOutOfBoundsException e) {
                weight3 = 0;
            }
            try {
                weight4 = listCriterias.get(4).getWeight();
            } catch (IndexOutOfBoundsException e) {
                weight4 = 0;
            }

            switch (listCriterias.size()) {
                case 1:
                    RunAHP.calculator(listFirst);
                    RunAHP.updateListAfterCalculator(weight, listFirst);
                    ketqua.setListResult(listFirst);
                    break;
                case 2:
                    RunAHP.calculator(listFirst);
                    RunAHP.updateListAfterCalculator(weight, listFirst);
                    RunAHP.calculator(listSecond);
                    RunAHP.updateListAfterCalculator(weight1, listSecond);
                    ketqua.setListResult(RunAHP.sumCriterias(listFirst, listSecond));
                    break;
                case 3:
                    RunAHP.calculator(listFirst);
                    RunAHP.updateListAfterCalculator(weight, listFirst);
                    RunAHP.calculator(listSecond);
                    RunAHP.updateListAfterCalculator(weight1, listSecond);
                    RunAHP.calculator(listThird);
                    RunAHP.updateListAfterCalculator(weight2, listThird);
                    ketqua.setListResult(RunAHP.sumCriterias(listFirst, listSecond, listThird));
                    break;
                case 4:
                    RunAHP.calculator(listFirst);
                    RunAHP.updateListAfterCalculator(weight, listFirst);
                    RunAHP.calculator(listSecond);
                    RunAHP.updateListAfterCalculator(weight1, listSecond);
                    RunAHP.calculator(listThird);
                    RunAHP.updateListAfterCalculator(weight2, listThird);
                    RunAHP.calculator(listFour);
                    RunAHP.updateListAfterCalculator(weight3, listFour);
                    ketqua.setListResult(RunAHP.sumCriterias(listFirst, listSecond, listThird, listFour));
                    break;
                case 5:
                    RunAHP.calculator(listFirst);
                    RunAHP.updateListAfterCalculator(weight, listFirst);
                    RunAHP.calculator(listSecond);
                    RunAHP.updateListAfterCalculator(weight1, listSecond);
                    RunAHP.calculator(listThird);
                    RunAHP.updateListAfterCalculator(weight2, listThird);
                    RunAHP.calculator(listFour);
                    RunAHP.updateListAfterCalculator(weight3, listFour);
                    RunAHP.calculator(listFive);
                    RunAHP.updateListAfterCalculator(weight4, listFive);
                    ketqua.setListResult(RunAHP.sumCriterias(listFirst, listSecond, listThird, listFour, listFive));
                    break;
            }
            ketqua.show();
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Matrix_phuongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Matrix_phuongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Matrix_phuongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Matrix_phuongan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Matrix_phuongan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableForm;
    // End of variables declaration//GEN-END:variables
}
