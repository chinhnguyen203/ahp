/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package giaodien;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import model.Criteria;

/**
 *
 * @author Admin
 */
public class MĐƯT_tieuchi2 extends javax.swing.JFrame {

    List<Criteria> critValue = new ArrayList<>();
    List<Criteria> cloneCritValue = new ArrayList<>();
    JTextField[] arrTextFields;
    JSlider[] arrJSliders;
    JCheckBox[] arrCheckBoxs;
    JPanel[] arrJPanels;
    JCheckBox selectedCheckBox;
    double compareResult[][];
    double weight[][];
    int critIndex;
    double[] RI_ARRAY = new double[][]{{0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 1.51, 1.54, 1.56, 1.57, 1.59}}[0];

    public MĐƯT_tieuchi2() {
        initComponents();

        this.arrTextFields = new JTextField[]{txtPrio1, txtPrio2};
        this.arrJSliders = new JSlider[]{jslPrio1, jslPrio2};
        this.arrCheckBoxs = new JCheckBox[]{ckbPrio1, ckbPrio2};
        this.arrJPanels = new JPanel[]{pnPrio1, pnPrio2};
    }

    public void setCritValue(List<Criteria> critValue) {
        this.critValue = critValue;
        initData();
        setVisibleJSliderAndJCheckBox();
        compareResult = new double[this.critValue.size()][this.critValue.size()];
    }

    public void setCompareResult(double[][] compareResult) {
        this.compareResult = compareResult;
    }

    public void setCloneCritValue(List<Criteria> cloneCritValue) {
        this.cloneCritValue = cloneCritValue;
        int size = this.cloneCritValue.size();
        weight = new double[size][size];
    }

    /**
     * set visible or invisible for JSlider and JCheckbox
     */
    private void setVisibleJSliderAndJCheckBox() {
        for (int i = 0; i < critValue.size(); i++) {
            if (critValue.get(i).isStatus() == true) {
                arrCheckBoxs[i].setVisible(true);
                selectedCheckBox = arrCheckBoxs[i];
                arrJSliders[i].setVisible(false);
            } else {
                arrCheckBoxs[i].setVisible(false);
            }
        }
    }

    /**
     * set fields
     */
    public void initData() {
        for (int i = 0; i < critValue.size(); i++) {
            arrTextFields[i].setText(critValue.get(i).getNameCriteria());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        pnPrio1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtPrio1 = new javax.swing.JTextField();
        jslPrio1 = new javax.swing.JSlider();
        pnPrio2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtPrio2 = new javax.swing.JTextField();
        jslPrio2 = new javax.swing.JSlider();
        jPanel5 = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mức độ ưu tiên");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Xác định mức độ ưu tiên của tiêu chí", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(204, 0, 0))); // NOI18N
        jPanel1.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Xác định mức độ ưu tiên các tiêu chí so với tiêu chí mốc");

        pnPrio1.setMinimumSize(new java.awt.Dimension(244, 62));

        jLabel11.setText("Tiêu chí 1");
        pnPrio1.add(jLabel11);

        txtPrio1.setMinimumSize(new java.awt.Dimension(30, 22));
        txtPrio1.setPreferredSize(new java.awt.Dimension(170, 30));
        pnPrio1.add(txtPrio1);

        jslPrio1.setMajorTickSpacing(1);
        jslPrio1.setMaximum(9);
        jslPrio1.setMinimum(1);
        jslPrio1.setPaintLabels(true);
        jslPrio1.setPaintTicks(true);
        jslPrio1.setToolTipText("");
        jslPrio1.setValue(1);
        jslPrio1.setAlignmentX(1.0F);
        jslPrio1.setAlignmentY(1.0F);
        jslPrio1.setEnabled(false);
        jslPrio1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslPrio1StateChanged(evt);
            }
        });
        pnPrio1.add(jslPrio1);

        pnPrio2.setMinimumSize(new java.awt.Dimension(244, 62));

        jLabel12.setText("Tiêu chí 2");
        pnPrio2.add(jLabel12);

        txtPrio2.setMinimumSize(new java.awt.Dimension(30, 22));
        txtPrio2.setPreferredSize(new java.awt.Dimension(170, 30));
        txtPrio2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrio2ActionPerformed(evt);
            }
        });
        pnPrio2.add(txtPrio2);

        jslPrio2.setMajorTickSpacing(1);
        jslPrio2.setMaximum(9);
        jslPrio2.setMinimum(1);
        jslPrio2.setPaintLabels(true);
        jslPrio2.setPaintTicks(true);
        jslPrio2.setToolTipText("");
        jslPrio2.setValue(1);
        jslPrio2.setAlignmentX(1.0F);
        jslPrio2.setAlignmentY(1.0F);
        jslPrio2.setEnabled(false);
        jslPrio2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslPrio2StateChanged(evt);
            }
        });
        pnPrio2.add(jslPrio2);

        jButton9.setText("Next");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setText("Back");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jButton10)
                .addGap(42, 42, 42)
                .addComponent(jButton9)
                .addContainerGap(55, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jButton10))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnPrio1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnPrio2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(109, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(94, 94, 94))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnPrio1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnPrio2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtPrio2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrio2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrio2ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        setCompareResult();
        double ratio = calcWeight();
        if (ratio > 10) {
            JOptionPane.showMessageDialog(this, "Tỷ số nhất quán: CR = " + ratio + "%" + "\n" + "Cần chọn lại mức độ các tiêu chí.");
        } else {
            JOptionPane.showMessageDialog(this, "Tỷ số nhất quán: CR = " + ratio + "%" + "\n" + "Chuyển sang bước nhập phương án.");
            setVisible(false);
            dispose();

//            setCompareResult();
            Nhap_so_phuongan pa = new Nhap_so_phuongan();
            pa.setCloneCritValue(cloneCritValue);
            pa.show();
        }
    }//GEN-LAST:event_jButton9ActionPerformed

    public void setCompareResult() {
        int m = critValue.size() - 1;
        DecimalFormat df = new DecimalFormat("0.0000");
        critValue.get(critIndex).setPriorityLevel(1);
        for (int j = 0; j <= m; j++) {
            compareResult[j][j] = 1.0;
            compareResult[m][j] = critValue.get(j).getPriorityLevel();
            double num = (double) 1 / critValue.get(j).getPriorityLevel();
            compareResult[j][m] = Double.valueOf(df.format(num));
        }
    }

    /**
     * Tính chỉ sô nhất quán
     *
     * @return double cr
     */
    public double calcWeight() {
        int m = cloneCritValue.size();
        double RI = RI_ARRAY[m - 1];
        DecimalFormat df = new DecimalFormat("0.0000");
        List<Double> colSum = new ArrayList<>(); // Giá trị tổng cột
        List<Double> lstWeightCrit = new ArrayList<>(); // Trọng số

        // Tính tổng cột
        for (int i = 0; i < m; i++) {
            double sum = 0;
            for (int j = 0; j < m; j++) {
                sum += compareResult[i][j];
            }
            colSum.add(sum);
        }

        // Xác định trọng số tiêu chí
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                weight[i][j] = Double.valueOf(df.format(compareResult[i][j] / colSum.get(i)));
            }
        }

        for (int i = 0; i < m; i++) {
            double sum = 0;
            int count = 0;
            for (int j = 0; j < m; j++) {
                sum += weight[j][i];
                count++;
            }
            double avg = Double.valueOf(df.format(sum / count));
            lstWeightCrit.add(avg);
        }
        // End xác định trọng số tiêu chí
        setCriteriaWeight(lstWeightCrit);

        double lamdaMax = 0;
        double ci;
        double cr;

        for (int i = 0; i < colSum.size(); i++) {
            lamdaMax += colSum.get(i) * lstWeightCrit.get(i);
        }
        ci = (double) (lamdaMax - m) / (m - 1);
        cr = (double) (ci / RI) * 100;
        return Double.valueOf(df.format(cr));
    }

    public void setCriteriaWeight(List<Double> lstWeightCrit) {
        int idx = cloneCritValue.size();
        for (int i = 0; i < idx; i++) {
            Criteria crit = cloneCritValue.get(i);
            crit.setWeight(lstWeightCrit.get(i));
            cloneCritValue.set(i, crit);
        }
    }

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        setVisible(false);
        dispose();
        tieuchi tieuchi = new tieuchi();
        tieuchi.show();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jslPrio1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslPrio1StateChanged
        critValue.get(0).setPriorityLevel(jslPrio1.getValue());
    }//GEN-LAST:event_jslPrio1StateChanged

    private void jslPrio2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslPrio2StateChanged
        critValue.get(1).setPriorityLevel(jslPrio2.getValue());
    }//GEN-LAST:event_jslPrio2StateChanged

    public void setVisibleJslider(JCheckBox checkBox) {
        for (int i = 0; i < critValue.size(); i++) {
            if (arrCheckBoxs[i] == checkBox && checkBox.isSelected()) {
                arrJSliders[i].setEnabled(false);
            } else if (!checkBox.isSelected()) {
                arrJSliders[i].setEnabled(false);
            } else {
                arrJSliders[i].setEnabled(true);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MĐƯT_tieuchi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MĐƯT_tieuchi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MĐƯT_tieuchi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MĐƯT_tieuchi2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MĐƯT_tieuchi2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSlider jslPrio1;
    private javax.swing.JSlider jslPrio2;
    private javax.swing.JPanel pnPrio1;
    private javax.swing.JPanel pnPrio2;
    private javax.swing.JTextField txtPrio1;
    private javax.swing.JTextField txtPrio2;
    // End of variables declaration//GEN-END:variables
}
