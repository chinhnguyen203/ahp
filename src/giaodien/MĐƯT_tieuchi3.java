package giaodien;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import model.Criteria;

public class MĐƯT_tieuchi3 extends javax.swing.JFrame {

    List<Criteria> critValue = new ArrayList<>();
    List<Criteria> cloneCritValue = new ArrayList<>();
    JTextField[] arrTextFields;
    JSlider[] arrJSliders;
    JCheckBox[] arrCheckBoxs;
    JPanel[] arrJPanels;
    JCheckBox selectedCheckBox;
    double compareResult[][];
    int critIndex;

    public MĐƯT_tieuchi3() {
        initComponents();

        this.arrTextFields = new JTextField[]{txtPrio1, txtPrio2, txtPrio3};
        this.arrJSliders = new JSlider[]{jslPrio1, jslPrio2, jslPrio3};
        this.arrCheckBoxs = new JCheckBox[]{ckbPrio1, ckbPrio2, ckbPrio3};
        this.arrJPanels = new JPanel[]{pnPrio1, pnPrio2, pnPrio3};
    }

    public void setCritValue(List<Criteria> critValue) {
        this.critValue = critValue;
        initData();
        setVisibleJSliderAndJCheckBox();
        compareResult = new double[this.critValue.size()][this.critValue.size()];
    }

    public void setCloneCritValue(List<Criteria> cloneCritValue) {
        this.cloneCritValue = cloneCritValue;
    }

    public void setCompareResult(double[][] compareResult) {
        this.compareResult = compareResult;
    }

    /**
     * set visible or invisible for JSlider and JCheckbox
     */
    private void setVisibleJSliderAndJCheckBox() {
        for (int i = 0; i < critValue.size(); i++) {
            if (critValue.get(i).isStatus() == true) {
                arrCheckBoxs[i].setVisible(true);
                selectedCheckBox = arrCheckBoxs[i];
                arrJSliders[i].setVisible(false);
            } else {
                arrCheckBoxs[i].setVisible(false);
            }
        }
    }

    /**
     * set fields
     */
    public void initData() {
        for (int i = 0; i < critValue.size(); i++) {
            arrTextFields[i].setText(critValue.get(i).getNameCriteria());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        pnPrio1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtPrio1 = new javax.swing.JTextField();
        jslPrio1 = new javax.swing.JSlider();
        pnPrio2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtPrio2 = new javax.swing.JTextField();
        jslPrio2 = new javax.swing.JSlider();
        pnPrio3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txtPrio3 = new javax.swing.JTextField();
        jslPrio3 = new javax.swing.JSlider();
        jPanel5 = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mức độ ưu tiên");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Xác định mức độ ưu tiên của tiêu chí", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(204, 0, 0))); // NOI18N
        jPanel1.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Xác định mức độ ưu tiên các tiêu chí so với tiêu chí mốc");

        pnPrio1.setMinimumSize(new java.awt.Dimension(244, 62));

        jLabel11.setText("Tiêu chí 1");
        pnPrio1.add(jLabel11);

        txtPrio1.setMinimumSize(new java.awt.Dimension(30, 22));
        txtPrio1.setPreferredSize(new java.awt.Dimension(170, 30));
        pnPrio1.add(txtPrio1);

        jslPrio1.setMajorTickSpacing(1);
        jslPrio1.setMaximum(9);
        jslPrio1.setMinimum(1);
        jslPrio1.setPaintLabels(true);
        jslPrio1.setPaintTicks(true);
        jslPrio1.setToolTipText("");
        jslPrio1.setValue(1);
        jslPrio1.setAlignmentX(1.0F);
        jslPrio1.setAlignmentY(1.0F);
        jslPrio1.setEnabled(false);
        jslPrio1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslPrio1StateChanged(evt);
            }
        });
        pnPrio1.add(jslPrio1);

        pnPrio2.setMinimumSize(new java.awt.Dimension(244, 62));

        jLabel12.setText("Tiêu chí 2");
        pnPrio2.add(jLabel12);

        txtPrio2.setMinimumSize(new java.awt.Dimension(30, 22));
        txtPrio2.setPreferredSize(new java.awt.Dimension(170, 30));
        pnPrio2.add(txtPrio2);

        jslPrio2.setMajorTickSpacing(1);
        jslPrio2.setMaximum(9);
        jslPrio2.setMinimum(1);
        jslPrio2.setPaintLabels(true);
        jslPrio2.setPaintTicks(true);
        jslPrio2.setToolTipText("");
        jslPrio2.setValue(1);
        jslPrio2.setAlignmentX(1.0F);
        jslPrio2.setAlignmentY(1.0F);
        jslPrio2.setEnabled(false);
        jslPrio2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslPrio2StateChanged(evt);
            }
        });
        pnPrio2.add(jslPrio2);

        pnPrio3.setMinimumSize(new java.awt.Dimension(244, 62));

        jLabel13.setText("Tiêu chí 3");
        pnPrio3.add(jLabel13);

        txtPrio3.setMinimumSize(new java.awt.Dimension(30, 22));
        txtPrio3.setPreferredSize(new java.awt.Dimension(170, 30));
        pnPrio3.add(txtPrio3);

        jslPrio3.setMajorTickSpacing(1);
        jslPrio3.setMaximum(9);
        jslPrio3.setMinimum(1);
        jslPrio3.setPaintLabels(true);
        jslPrio3.setPaintTicks(true);
        jslPrio3.setToolTipText("");
        jslPrio3.setValue(1);
        jslPrio3.setAlignmentX(1.0F);
        jslPrio3.setAlignmentY(1.0F);
        jslPrio3.setEnabled(false);
        jslPrio3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jslPrio3StateChanged(evt);
            }
        });
        pnPrio3.add(jslPrio3);

        jButton9.setText("Next");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setText("Back");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jButton10)
                .addGap(42, 42, 42)
                .addComponent(jButton9)
                .addContainerGap(56, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton9)
                    .addComponent(jButton10)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnPrio1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnPrio2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnPrio3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(32, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(52, 52, 52))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnPrio1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnPrio2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnPrio3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(87, 87, 87))
        );

        setSize(new java.awt.Dimension(550, 453));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        setVisible(false);
        dispose();

        setCompareResult();
        for (Criteria criteria : critValue) {
            if (criteria.isStatus()) {
                critValue.remove(criteria);
                break;
            }
        }
        findLowestPriorityLevel();

        MĐƯT_tieuchi2 t_tieuchi2 = new MĐƯT_tieuchi2();
        t_tieuchi2.setCritValue(critValue);
        t_tieuchi2.setCompareResult(compareResult);
        t_tieuchi2.setCloneCritValue(cloneCritValue);
        t_tieuchi2.show();
    }//GEN-LAST:event_jButton9ActionPerformed

    public void setCompareResult() {
        int m = critValue.size() - 1;
        DecimalFormat df = new DecimalFormat("0.0000");
        critValue.get(critIndex).setPriorityLevel(1);
        for (int j = 0; j <= m; j++) {
            compareResult[j][j] = 1.0;
            compareResult[m][j] = critValue.get(j).getPriorityLevel();
            double num = (double) 1 / critValue.get(j).getPriorityLevel();
            compareResult[j][m] = Double.valueOf(df.format(num));
        }
    }

    public void findLowestPriorityLevel() {
        List<Integer> lstNum = new ArrayList<>();
        critValue.stream().forEach((critVal) -> {
            lstNum.add(critVal.getCriticalLevel());
        });
        Collections.sort(lstNum);

        int maxCriticalValue = lstNum.get(lstNum.size() - 1);
        for (Criteria critVal : critValue) {
            critVal.setStatus(false);
            if (critVal.getCriticalLevel() == maxCriticalValue) {
                critVal.setStatus(true);
            }
        }
    }

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        dispose();
        tieuchi tieuchi = new tieuchi();
        tieuchi.show();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jslPrio1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslPrio1StateChanged
        critValue.get(0).setPriorityLevel(jslPrio1.getValue());
    }//GEN-LAST:event_jslPrio1StateChanged

    private void jslPrio2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslPrio2StateChanged
        critValue.get(1).setPriorityLevel(jslPrio2.getValue());
    }//GEN-LAST:event_jslPrio2StateChanged

    private void jslPrio3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jslPrio3StateChanged
        critValue.get(2).setPriorityLevel(jslPrio3.getValue());
    }//GEN-LAST:event_jslPrio3StateChanged

    public void setVisibleJslider(JCheckBox checkBox) {
        for (int i = 0; i < critValue.size(); i++) {
            if (arrCheckBoxs[i] == checkBox && checkBox.isSelected()) {
                arrJSliders[i].setEnabled(false);
            } else if (!checkBox.isSelected()) {
                arrJSliders[i].setEnabled(false);
            } else {
                arrJSliders[i].setEnabled(true);
            }
        }
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MĐƯT_tieuchi3().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSlider jslPrio1;
    private javax.swing.JSlider jslPrio2;
    private javax.swing.JSlider jslPrio3;
    private javax.swing.JPanel pnPrio1;
    private javax.swing.JPanel pnPrio2;
    private javax.swing.JPanel pnPrio3;
    private javax.swing.JTextField txtPrio1;
    private javax.swing.JTextField txtPrio2;
    private javax.swing.JTextField txtPrio3;
    // End of variables declaration//GEN-END:variables
}
