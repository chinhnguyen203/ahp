package giaodien;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Criteria;
import model.Plan;

public class Nhap_so_phuongan extends javax.swing.JFrame {

    List<JPanel> listPanel;
    List<Plan> listPlan = new ArrayList<>();
    // Danh sách tiêu chí
    List<Criteria> cloneCritValue = new ArrayList<>();
    JTextField[] arrTextFields;
    int count = 10;

    public Nhap_so_phuongan() {

        initComponents();
        this.arrTextFields = new JTextField[]{edtPlan1, edtPlan2, edtPlan3, edtPlan4, edtPlan5, edtPlan6, edtPlan7, edtPlan8, edtPlan9, edtPlan10};

        listPanel = new ArrayList<>();
        listPanel.add(panel1);
        listPanel.add(panel2);
        listPanel.add(panel3);
        listPanel.add(panel4);
        listPanel.add(panel5);
        listPanel.add(panel6);
        listPanel.add(panel7);
        listPanel.add(panel8);
        listPanel.add(panel9);
        listPanel.add(panel10);

    }

    public void setCloneCritValue(List<Criteria> cloneCritValue) {
        this.cloneCritValue = cloneCritValue;
    }

    private void getPlantFromEdt() {
        for (int i = 0; i < arrTextFields.length; i++) {
            if (i < count) {
                listPlan.add(new Plan(i + "", arrTextFields[i].getText()));
            }
        }
    }

    private boolean checkValidate() {
        for (int i = 0; i < arrTextFields.length; i++) {
            if (i < count && arrTextFields[i].getText().trim().equals("")) {
                JOptionPane.showMessageDialog(this, "Please enter values!");
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAll = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        edtPhuongAn = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Phương án");

        jPanelAll.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin phương án", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14), new java.awt.Color(204, 0, 0))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Nhập số phương án");

        edtPhuongAn.setText("10");
        edtPhuongAn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtPhuongAnActionPerformed(evt);
            }
        });
        edtPhuongAn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                edtPhuongAnKeyReleased(evt);
            }
        });

        jButton1.setText("Next");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelAllLayout = new javax.swing.GroupLayout(jPanelAll);
        jPanelAll.setLayout(jPanelAllLayout);
        jPanelAllLayout.setHorizontalGroup(
            jPanelAllLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAllLayout.createSequentialGroup()
                .addGap(146, 146, 146)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(edtPhuongAn, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(107, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAllLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );
        jPanelAllLayout.setVerticalGroup(
            jPanelAllLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAllLayout.createSequentialGroup()
                .addGap(88, 88, 88)
                .addGroup(jPanelAllLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(edtPhuongAn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(64, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelAll, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelAll, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanelAll.getAccessibleContext().setAccessibleName("Phương án");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void edtPhuongAnKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtPhuongAnKeyReleased
        if (edtPhuongAn.getText().equals("")) {
            return;
        }
        try {
            count = Integer.parseInt(edtPhuongAn.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Please enter number");
            count = 1;
            edtPhuongAn.setText(count + "");
        }
        if (count > 10 || count < 1) {
            JOptionPane.showMessageDialog(this, "Number between [1 - 10]");
            edtPhuongAn.setText("10");
        } else {
            for (int i = 0; i < 10; i++) {
                if (i < count) {
                    listPanel.get(i).setVisible(true);
                } else {
                    listPanel.get(i).setVisible(false);
                }
            }
        }
    }//GEN-LAST:event_edtPhuongAnKeyReleased

    private void edtPhuongAnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtPhuongAnActionPerformed

    }//GEN-LAST:event_edtPhuongAnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (!checkValidate()) {
            return;
        }
        setVisible(false);
        dispose();
        getPlantFromEdt();
        Matrix_phuongan Matrix_phuongan = new Matrix_phuongan();
        Matrix_phuongan.setListCriterias(cloneCritValue);
        Matrix_phuongan.setNumberPlan(count);
        Matrix_phuongan.setListPlan(listPlan);
        Matrix_phuongan.show();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        dispose();
        MĐƯT_tieuchi2 MĐƯT_tieuchi2 = new MĐƯT_tieuchi2();
        MĐƯT_tieuchi2.show();
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Nhap_so_phuongan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField edtPhuongAn;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelAll;
    // End of variables declaration//GEN-END:variables
}
