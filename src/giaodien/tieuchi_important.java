/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package giaodien;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Criteria;
import model.Priority;

/**
 *
 * @author Admin
 */
public final class tieuchi_important extends javax.swing.JFrame {

    /**
     * Creates new form tieuchi
     */
    List<JPanel> jPanels = new ArrayList<>();
    JTextField[] arrTextFields;
    JComboBox[] arrCmbBoxs;
    List<Priority> priorities = setListPriority();
    List<Criteria> critValue = new ArrayList<>();
    List<Criteria> cloneCritValue = new ArrayList<>();

    public List<Criteria> getCritValue() {
        return critValue;
    }

    public List<Criteria> getCloneCritValue() {
        return cloneCritValue;
    }

    public tieuchi_important() {
        initComponents();
        lstPanel();
        this.arrTextFields = new JTextField[]{txtFirstCri1, txtFirstCri2, txtFirstCri3, txtFirstCri4, txtFirstCri5};
        this.arrCmbBoxs = new JComboBox[]{cmbCrit1, cmbCrit2, cmbCrit3, cmbCrit4, cmbCrit5};

        for (int i = 0; i < 5; i++) {
            arrCmbBoxs[i].addItem(priorities.get(0).getPriority());
            arrCmbBoxs[i].addItem(priorities.get(1).getPriority());
            arrCmbBoxs[i].addItem(priorities.get(2).getPriority());
            arrCmbBoxs[i].addItem(priorities.get(3).getPriority());
            arrCmbBoxs[i].addItem(priorities.get(4).getPriority());
        }

    }

    public List<Priority> setListPriority() {
        List<Priority> list = new ArrayList<>();
        list.add(new Priority("Fisrt priority", 1));
        list.add(new Priority("Second priority", 2));
        list.add(new Priority("Third priority", 3));
        list.add(new Priority("Fourth priority", 4));
        list.add(new Priority("Fifth priority", 5));

        return list;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlCriteria = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnCri1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtFirstCri1 = new javax.swing.JTextField();
        cmbCrit1 = new javax.swing.JComboBox<>();
        pnCri2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtFirstCri2 = new javax.swing.JTextField();
        cmbCrit2 = new javax.swing.JComboBox<>();
        pnCri4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtFirstCri4 = new javax.swing.JTextField();
        cmbCrit4 = new javax.swing.JComboBox<>();
        pnCri3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtFirstCri3 = new javax.swing.JTextField();
        cmbCrit3 = new javax.swing.JComboBox<>();
        pnCri5 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtFirstCri5 = new javax.swing.JTextField();
        cmbCrit5 = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        btnCritBack = new javax.swing.JButton();
        btnCritNext = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tiêu chí");
        setName("tiêu chí"); // NOI18N

        pnlCriteria.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin tiêu chí", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(204, 0, 0))); // NOI18N
        pnlCriteria.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Chọn mức độ");
        pnlCriteria.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(421, 82, -1, -1));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setText("Xác định thứ tự quan trọng của các tiêu chí");
        pnlCriteria.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 40, 370, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Tên tiêu chí");
        pnlCriteria.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(172, 82, 130, -1));

        jLabel5.setText("Tiêu chí 1");
        jLabel5.setPreferredSize(new java.awt.Dimension(56, 25));

        txtFirstCri1.setPreferredSize(new java.awt.Dimension(49, 30));

        cmbCrit1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
        cmbCrit1.setPreferredSize(new java.awt.Dimension(111, 30));
        cmbCrit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCrit1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCri1Layout = new javax.swing.GroupLayout(pnCri1);
        pnCri1.setLayout(pnCri1Layout);
        pnCri1Layout.setHorizontalGroup(
            pnCri1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(txtFirstCri1, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(cmbCrit1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        pnCri1Layout.setVerticalGroup(
            pnCri1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cmbCrit1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtFirstCri1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlCriteria.add(pnCri1, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 115, 528, -1));

        jLabel6.setText("Tiêu chí 2");
        jLabel6.setPreferredSize(new java.awt.Dimension(56, 25));

        txtFirstCri2.setPreferredSize(new java.awt.Dimension(49, 30));

        cmbCrit2.setPreferredSize(new java.awt.Dimension(111, 30));
        cmbCrit2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCrit2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCri2Layout = new javax.swing.GroupLayout(pnCri2);
        pnCri2.setLayout(pnCri2Layout);
        pnCri2Layout.setHorizontalGroup(
            pnCri2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(txtFirstCri2, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(cmbCrit2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        pnCri2Layout.setVerticalGroup(
            pnCri2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cmbCrit2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtFirstCri2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlCriteria.add(pnCri2, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 152, -1, -1));

        jLabel7.setText("Tiêu chí 4");
        jLabel7.setPreferredSize(new java.awt.Dimension(56, 25));

        txtFirstCri4.setPreferredSize(new java.awt.Dimension(49, 30));

        cmbCrit4.setPreferredSize(new java.awt.Dimension(111, 30));
        cmbCrit4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCrit4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCri4Layout = new javax.swing.GroupLayout(pnCri4);
        pnCri4.setLayout(pnCri4Layout);
        pnCri4Layout.setHorizontalGroup(
            pnCri4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(txtFirstCri4, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(cmbCrit4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        pnCri4Layout.setVerticalGroup(
            pnCri4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cmbCrit4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtFirstCri4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlCriteria.add(pnCri4, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 229, -1, -1));

        jLabel8.setText("Tiêu chí 3");
        jLabel8.setPreferredSize(new java.awt.Dimension(56, 25));

        txtFirstCri3.setPreferredSize(new java.awt.Dimension(49, 30));

        cmbCrit3.setPreferredSize(new java.awt.Dimension(111, 30));
        cmbCrit3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCrit3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCri3Layout = new javax.swing.GroupLayout(pnCri3);
        pnCri3.setLayout(pnCri3Layout);
        pnCri3Layout.setHorizontalGroup(
            pnCri3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(txtFirstCri3, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(cmbCrit3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        pnCri3Layout.setVerticalGroup(
            pnCri3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cmbCrit3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtFirstCri3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlCriteria.add(pnCri3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 189, -1, -1));

        jLabel9.setText("Tiêu chí 5");
        jLabel9.setPreferredSize(new java.awt.Dimension(56, 25));

        txtFirstCri5.setPreferredSize(new java.awt.Dimension(49, 30));

        cmbCrit5.setPreferredSize(new java.awt.Dimension(111, 30));
        cmbCrit5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCrit5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCri5Layout = new javax.swing.GroupLayout(pnCri5);
        pnCri5.setLayout(pnCri5Layout);
        pnCri5Layout.setHorizontalGroup(
            pnCri5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addComponent(txtFirstCri5, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(cmbCrit5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        pnCri5Layout.setVerticalGroup(
            pnCri5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCri5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cmbCrit5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtFirstCri5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlCriteria.add(pnCri5, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 266, -1, -1));

        btnCritBack.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCritBack.setText("Back");
        btnCritBack.setPreferredSize(new java.awt.Dimension(65, 30));
        btnCritBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCritBackActionPerformed(evt);
            }
        });

        btnCritNext.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnCritNext.setText("Next");
        btnCritNext.setPreferredSize(new java.awt.Dimension(65, 30));
        btnCritNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCritNextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(173, 173, 173)
                .addComponent(btnCritBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addComponent(btnCritNext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(138, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCritBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCritNext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pnlCriteria.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 320, 490, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCriteria, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCritNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCritNextActionPerformed
        int numCriteria = Integer.parseInt(lblCriteriaQuantity.getText());
        
        for (int i = 0; i < numCriteria; i++) {
            Criteria c = new Criteria();
            c.setIdCriteria(String.valueOf(i + 1));
            c.setNameCriteria(arrTextFields[i].getText());
            c.setCriticalLevel(arrCmbBoxs[i].getSelectedIndex() + 1);

            critValue.add(c);
        }

        findLowestPriorityLevel();

        setVisible(false);
        dispose();
        nextFrame();
    }//GEN-LAST:event_btnCritNextActionPerformed

    // Chuyển frame
    public void nextFrame(){
        int critSize = critValue.size();
        cloneCritValue.addAll(critValue);
        if (critSize == 5) {
            MDUT_tieuchi5 t_tieuchi5 = new MDUT_tieuchi5();
            t_tieuchi5.setCritValue(critValue);
            t_tieuchi5.setCloneCritValue(cloneCritValue);
            t_tieuchi5.show();
        }
        if (critSize == 4) {
            MĐUT_tieuchi4 t_tieuchi = new MĐUT_tieuchi4();
            t_tieuchi.setCritValue(critValue);
            t_tieuchi.setCloneCritValue(cloneCritValue);
            t_tieuchi.show();
        }
        if (critSize == 3) {
            MĐƯT_tieuchi3 t_tieuchi1 = new MĐƯT_tieuchi3();
            t_tieuchi1.setCritValue(critValue);
            t_tieuchi1.setCloneCritValue(cloneCritValue);
            t_tieuchi1.show();
        }
        if (critSize == 2) {
            MĐƯT_tieuchi2 t_tieuchi2 = new MĐƯT_tieuchi2();
            t_tieuchi2.setCritValue(critValue);
            t_tieuchi2.setCloneCritValue(cloneCritValue);
            t_tieuchi2.show();
        }
    }
    
    // Tìm độ quan trọng thấp nhất
    public void findLowestPriorityLevel(){
        List<Integer> lstNum = new ArrayList<>();
        critValue.stream().forEach((critVal) -> {
            lstNum.add(critVal.getCriticalLevel());
        });
        Collections.sort(lstNum);

        int maxCriticalValue = lstNum.get(lstNum.size() - 1);
        for (Criteria critVal : critValue) {
            critVal.setStatus(false);
            if (critVal.getCriticalLevel() == maxCriticalValue) {
                critVal.setStatus(true);
            }
        }
    }
    
    private void btnCritBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCritBackActionPerformed
        // TODO add your handling code here:
        setVisible(false);
        dispose();
        MenuUser choicedata = new MenuUser();
        choicedata.show();
    }//GEN-LAST:event_btnCritBackActionPerformed

    public void lstPanel() {
        jPanels.add(pnCri1);
        jPanels.add(pnCri2);
        jPanels.add(pnCri3);
        jPanels.add(pnCri4);
        jPanels.add(pnCri5);
    }

    private void cmbCrit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCrit1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCrit1ActionPerformed

    private void cmbCrit2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCrit2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCrit2ActionPerformed

    private void cmbCrit4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCrit4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCrit4ActionPerformed

    private void cmbCrit3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCrit3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCrit3ActionPerformed

    private void cmbCrit5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCrit5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCrit5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(tieuchi_important.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(tieuchi_important.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(tieuchi_important.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(tieuchi_important.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new tieuchi_important().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCritBack;
    private javax.swing.JButton btnCritNext;
    private javax.swing.JComboBox<String> cmbCrit1;
    private javax.swing.JComboBox<String> cmbCrit2;
    private javax.swing.JComboBox<String> cmbCrit3;
    private javax.swing.JComboBox<String> cmbCrit4;
    private javax.swing.JComboBox<String> cmbCrit5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel pnCri1;
    private javax.swing.JPanel pnCri2;
    private javax.swing.JPanel pnCri3;
    private javax.swing.JPanel pnCri4;
    private javax.swing.JPanel pnCri5;
    private javax.swing.JPanel pnlCriteria;
    private javax.swing.JTextField txtFirstCri1;
    private javax.swing.JTextField txtFirstCri2;
    private javax.swing.JTextField txtFirstCri3;
    private javax.swing.JTextField txtFirstCri4;
    private javax.swing.JTextField txtFirstCri5;
    // End of variables declaration//GEN-END:variables
}
