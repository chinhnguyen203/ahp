/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author quocc
 */
public class Priority {
    private String priority;
    private int criticalLevel;

    public Priority() {
    }

    public Priority(String priority, int criticalLevel) {
        this.priority = priority;
        this.criticalLevel = criticalLevel;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getCriticalLevel() {
        return criticalLevel;
    }

    public void setCriticalLevel(int criticalLevel) {
        this.criticalLevel = criticalLevel;
    }
    
}
