/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package run;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PHD
 */
public class RunAHP {

    static List<Double> listFirst = new ArrayList<>();
    static List<Double> listSecond = new ArrayList<>();
    static List<Double> listThird = new ArrayList<>();
    static List<Double> listFour = new ArrayList<>();
    static List<Double> listFive = new ArrayList<>();

    private static int numberCriteria = 4;

    public static void main(String[] args) {
        initData();
        System.out.println(getMax(listFirst) + "");
        xuat(listFirst);
        calculator(listFirst);
        calculator(listSecond);
        calculator(listThird);
        calculator(listFour);
        //calculator(listFive);
        xuat(listFirst);
        xuat(listSecond);
        xuat(listThird);
        xuat(listFour);
        System.out.println("");
        System.out.println("");
        updateListAfterCalculator(0.547, listFirst);
        updateListAfterCalculator(0.127, listSecond);
        updateListAfterCalculator(0.27, listThird);
        updateListAfterCalculator(0.056, listFour);
        //updateListAfterCalculator(0.547, listFive);
        xuat(listFirst);
        xuat(listSecond);
        xuat(listThird);
        xuat(listFour);
        System.out.println("");
        //sumCriterias();
    }

    public static void initData() {
        listFirst.add(Double.valueOf(8500000));
        listFirst.add(Double.valueOf(5000000));
        listFirst.add(Double.valueOf(6500000));
        listFirst.add(Double.valueOf(10000000));
        listFirst.add(Double.valueOf(7500000));
        listFirst.add(Double.valueOf(6500000));
        listFirst.add(Double.valueOf(4000000));
        listFirst.add(Double.valueOf(6000000));
        listFirst.add(Double.valueOf(5000000));
        listFirst.add(Double.valueOf(9000000));

        listSecond.add(Double.valueOf(30));
        listSecond.add(Double.valueOf(40));
        listSecond.add(Double.valueOf(50));
        listSecond.add(Double.valueOf(45));
        listSecond.add(Double.valueOf(25));
        listSecond.add(Double.valueOf(30));
        listSecond.add(Double.valueOf(35));
        listSecond.add(Double.valueOf(40));
        listSecond.add(Double.valueOf(55));
        listSecond.add(Double.valueOf(60));

        listThird.add(Double.valueOf(5));
        listThird.add(Double.valueOf(4));
        listThird.add(Double.valueOf(6));
        listThird.add(Double.valueOf(8));
        listThird.add(Double.valueOf(4.5));
        listThird.add(Double.valueOf(8.5));
        listThird.add(Double.valueOf(7));
        listThird.add(Double.valueOf(6.5));
        listThird.add(Double.valueOf(9.5));
        listThird.add(Double.valueOf(5));

        listFour.add(Double.valueOf(4));
        listFour.add(Double.valueOf(6));
        listFour.add(Double.valueOf(4.5));
        listFour.add(Double.valueOf(8));
        listFour.add(Double.valueOf(7.5));
        listFour.add(Double.valueOf(9));
        listFour.add(Double.valueOf(8.5));
        listFour.add(Double.valueOf(6.5));
        listFour.add(Double.valueOf(7));
        listFour.add(Double.valueOf(8));
    }

    public static double getMax(List<Double> list) {
        double temp = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (temp < list.get(i)) {
                temp = list.get(i);
            }
        }
        return temp;
    }

    public static int getPositionMax(List<Double> list) {
        int position = 0;
        for (int i = 1; i < list.size(); i++) {
            if (getMax(list) == list.get(i)) {
                return i;
            }
        }
        return position;
    }

    public static void calculator(List<Double> list) {
        double max = getMax(list);
        for (int i = 0; i < list.size(); i++) {
            double result = list.get(i) / max;
            list.set(i, (double) Math.round(result * 100) / 100);
        }
    }

    public static void updateListAfterCalculator(double number, List<Double> list) {
        for (int i = 0; i < list.size(); i++) {
            double result = number * list.get(i);
            list.set(i, (double) Math.round(result * 10000) / 10000);
        }
    }

    public static void xuat(List<Double> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println("");
    }

    public List<Double> sumCriterias() {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            switch (numberCriteria) {
                case 2:
                    sum += listSecond.get(i);
                    break;
                case 3:
                    sum += listSecond.get(i) + listThird.get(i);
                    break;
                case 4:
                    sum += listSecond.get(i) + listThird.get(i) + listFour.get(i);
                    break;
                case 5:
                    sum = listSecond.get(i) + listThird.get(i) + listFour.get(i) + listFive.get(i);
                    break;
            }
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }

    public List<Double> sumCriterias(List<Double> listFirst) {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }

    public static List<Double> sumCriterias(List<Double> listFirst, List<Double> listSecond) {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            sum += listSecond.get(i);
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }

    public static List<Double> sumCriterias(List<Double> listFirst, List<Double> listSecond, List<Double> listThird) {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            sum += listSecond.get(i) + listThird.get(i);
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }

    public static List<Double> sumCriterias(List<Double> listFirst, List<Double> listSecond, List<Double> listThird, List<Double> listFour) {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            sum += listSecond.get(i) + listThird.get(i) + listFour.get(i);
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }

    public static List<Double> sumCriterias(List<Double> listFirst, List<Double> listSecond, List<Double> listThird, List<Double> listFour, List<Double> listFive) {
        List<Double> listResult = new ArrayList<>();
        for (int i = 0; i < listFirst.size(); i++) {
            double sum = listFirst.get(i);
            sum = listSecond.get(i) + listThird.get(i) + listFour.get(i) + listFive.get(i);
            listResult.add((double) Math.round(sum * 1000) / 1000);
        }
        return listResult;
    }
}
